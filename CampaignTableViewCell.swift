//
//  CampaignTableViewCell.swift
//  Swift3JsonParse
//
//  Created by Kerim Çağlar on 09/03/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class CampaignTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellBgImage: UIImageView!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var campaignName: UILabel!
    
    @IBOutlet weak var remainTime: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
