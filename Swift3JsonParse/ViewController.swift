//
//  ViewController.swift
//  Swift3JsonParse
//
//  Created by Kerim Çağlar on 07/03/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var baslik = [String]()
    var images = [String]()
    var expireDate = [String]()
    
    let now:Int32 = Int32(NSDate().timeIntervalSince1970) + 3*60*60
    
    @IBOutlet weak var tblView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // [] -> Array
        // {} -> Object/Nesne ~ Dictinary
        
        veriAl(urlString: "http://www.bucayapimarket.com/json.php")
    }
    
    func veriAl(urlString:String)
    {
        
        //let urlString = "http://www.bucayapimarket.com/json.php"
        let url = URL(string: urlString) // url formatına çevirme
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error != nil{ // hata varsa
                print(error!)
            }
                
            else
            {
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                    
                    if let jsonDic = json
                    {
                        for i in 0..<jsonDic.count
                        {
                            if let basliklar = jsonDic[i] as? NSDictionary
                            {
                                
                                if let baslikArray = basliklar["baslik"] as? String
                                {
                                    self.baslik.append(baslikArray)
                                }
                                
                                if let productImage = basliklar["resim"] as? String
                                {
                                    self.images.append(productImage)
                                }
                                
                                if let campaignEndDate = basliklar["tarih"] as? String
                                {
                                    self.expireDate.append(campaignEndDate)
                                    
                                }
                            }
                        }
                        
                        
                    }
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                    
                }
                    
                catch
                {
                    print(error)
                }
            }
        }
        
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // TableView Methodları DataSource
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return baslik.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for:indexPath) as! CampaignTableViewCell
        
        cell.contentView.backgroundColor = UIColor.clear
        
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 10, y: 8, width: self.view.frame.size.width - 20, height: 110))
        
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 5.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.sd_setImage(with: URL(string: images[indexPath.row]))
        
        cell.campaignName.text = baslik[indexPath.row]
        
        //Kampanya tarihi hesaplama
        let diffDate = Double(Int32((self.expireDate[indexPath.row]))! - (self.now))
        let days = floor(Double(diffDate / 86400))
        let hours = floor(Double(diffDate-days*86400)/(60*60))
        let mins = floor(Double((diffDate-(days*86400+hours*3600))/60))
        let secs = (diffDate) - (days*86400+hours*3600+mins*60)
        
        print("**********Zaman Farkı: \(days) gün \(hours) saat \(mins) dakika \(secs) saniye")
        
        if days>=5 {
            cell.remainTime.text = "Zamanın bitmesine 5+ gün var"
        }
            
        else if days == 0 {
            
            let zaman = String(format:"%02i:%02i:%02i", Int(hours), Int(mins), Int(secs))
            cell.remainTime.text = zaman
        }
            
        else if (days < 0)
        {
            cell.remainTime.text = "KAMPANYA SONA ERMİŞTİR"
        }
            
        else
        {
            cell.remainTime.text = "\(Int(days)) gün \(Int(hours)) saat \(Int(mins)) dakika \(Int(secs)) saniye"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        print("Satır Seçildi")
    }
    
    
}
